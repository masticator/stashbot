import ssl as _ssl
import irc.bot
import irc.connection
import logging

class RelayBot(irc.bot.SingleServerIRCBot):

    def __init__(self, channel, nickname, server, port=6667, password=None, ssl=False):
        params = ([(server, port, password)], nickname, nickname, )
        kwparams = {}
        if(ssl):
            kwparams['connect_factory'] = irc.connection.Factory(wrapper=_ssl.wrap_socket)
        irc.bot.SingleServerIRCBot.__init__(self, *params, **kwparams)
        self.channel = channel

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

    def send(self, msg):
        self.connection.privmsg(self.channel, msg)

    def on_join(self, c, e):
        print('joined ' + str(e.target))

