Stashbot
------------
A simple IRC bot for receiving commit messages from [Atlassian Stash](https://www.atlassian.com/software/stash) via IRC.  Runs a stand-alone web server for receiving POST requests from the Stash web hook, extracts information from the JSON payload, and sends formatted messages to the configured channel of an IRC server.


####Requirements:
* The [POST service webhook](https://confluence.atlassian.com/display/STASH/POST+service+webhook+for+Stash) for Stash.
* Python 2.6+
* Python IRC library (`sudo pip install irc`)


####Getting started
* Run the server once to create a config file with default values (`./server.py`).
* Exit the server (Ctrl-C).
* Edit the config file (config.py).  Alternately you can simply edit the defaults in the code before running the server for the first time.
* Run the server again.
* Configure the Stash POST web hook to point to your now-running web server.
* Enjoy.


