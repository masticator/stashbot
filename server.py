#!/usr/bin/python

import sys
import os
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
import cgi
import json
import relay
import ast
import threading
import logging

def get_config(filename='config.py'):
    ''' Returns a dict with configuration information.  Read from a file if
        available, or defaults otherwise. '''
    try:
        with open(filename, 'r') as f:
            s = f.read()
    except IOError as e:
        # Create a default config dict
        config = {
            'web server': {
                'addr': '127.0.0.1',
                'port': 4444,
            },
            'irc client': {
                'server': 'chat.freenode.net',
                'port': 6667,
                'channel': '#test',
                'nick': 'stashbot',
                'password': '',
                'ssl': False,
            },
            'output': {
                'summary header': True,
                'include link': True,
                'trim link': True,
                'stash url': 'http://stash.local',
                'commit_limit': 5,
            },
            'general': {
                'logging': True,
            }
        }
        # Save the default config to file with some line breaks for readability
        with open(filename, 'w') as f:
            f.write(str(config).replace(': {', ': {\n    ').replace('}, ', '},\n  '))
    else:
        config = ast.literal_eval(s) # Convert string to dict object

    return config

last_request = ''

class ServerHandler(BaseHTTPRequestHandler):
    ''' Very basic HTTP POST request handler.  Takes a JSON payload with Stash
        commit information, formats and sends selected bits to the IRC bot. '''

    log_all_posts = False

    def do_GET(self):
        logging.debug("======= GET REQUEST =======")
        logging.debug(self.headers)

    def do_POST(self):
        global last_request
        logging.debug("======= POST REQUEST =======")
        logging.debug(self.headers)
        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
        if ctype == 'multipart/form-data':
            data = cgi.parse_multipart(self.rfile, pdict)
        else:
            logging.debug('ctype: ' + str(ctype))
            length = int(self.headers.getheader('content-length'))
            data_str = self.rfile.read(length)

        # Sometimes we receive duplicate POSTs from Stash.  Ignore them
        if(data_str == last_request):
            logging.debug('Duplicate request, ignoring.')
            return

        last_request = data_str

        logging.debug("======= POST DATA =======")
        logging.debug(data_str)
        logging.debug("\n")
        
        if(self.log_all_posts):
            i = 0
            force = True
            while(force or os.path.isfile(s)):
                force = False
                i += 1
                s = str(i).zfill(4) + '.json'
            with open(s, 'w') as f:
                f.write(data_str)

        try:
            data = json.loads(data_str)
        except ValueError as e:
            logging.exception(e)
        else:
            # Save a record of the transmission
            with open('last.json', 'w') as f:
                f.write(data_str)

            self._process_data(data)

    def _process_data(self, data):
        # See if there are any updates
        show_header = self.config['output']['summary header']
        updates = False
        if(show_header):
            for ref in data['refChanges']:
                if(ref['type'] == 'UPDATE'):
                    updates = True
                    break
        repo = data['repository']
        commit_count = data['changesets']['size']
        # See http://forum.mphca.net/showthread.php?157724-IRC-Raw-text-markup-characters
        # for help with colors and other formatting.
        # Header
        if(show_header and updates):
            s = u'[%s] %d new commit%s to \u0002%s\u000F' % (repo['project']['name'],
                    commit_count, '' if commit_count == 1 else 's',
                    repo['name'])
            self.bot.send(s)
            logging.debug(s)
        for ref in data['refChanges']:
            ref_type = ref['type']
            if(ref_type == 'DELETE'):
                self._process_delete(data, ref)
            elif(ref_type == 'ADD'):
                self._process_add(data, ref)
        # If no commits show the action performed on the branch
        if(updates):
            # Show commits
            cnt = 0
            max_commits = self.config['output']['commit_limit']
            for value in data['changesets']['values']:
                commit = value['toCommit']
                branch_name = ''
                action = ''
                commit_id = commit['id']
                # Try to find a changed branch to match this commit
                for ref in data['refChanges']:
                    if(ref['toHash'] == commit_id):
                        branch_name = ref['refId']
                        action = ref['type']
                        if(branch_name.startswith('refs/heads/')):
                            branch_name = branch_name[len('refs/heads/'):] # Trim
                        break
                if(not branch_name): # Ignore previous commits
                    continue
                cnt += 1
                if(cnt > max_commits):
                    break
                # Linefeeds are not allowed in IRC messages.
                message = commit['message'].replace('\n', ' | ')
                # Total IRC message length cannot exceed 512 chars.
                if(len(message) > 300):
                    message = message[:300] + '...'
                s = u'%s/%s %s \u0002\u000312%s\u0003\u000F: \u0002\u00033%s\u0003\u000F (%d changes)' % (
                    repo['name'], branch_name, commit['displayId'],
                    commit['author']['name'], message,
                    len(value['changes']['values']),)
                if(self.config['output']['include link']):
                    # Include a link to view the commit
                    link = self.config['output']['stash url'] + value['link']['url']
                    if(self.config['output']['trim link']):
                        link = link[:link.find('#')] # Remove filename
                    s += ': ' + link
                self.bot.send(s)
                logging.debug(s)

    def _process_delete(self, data, ref):
        branch_name = ref['refId']
        if(branch_name.startswith('refs/heads/')):
            branch_name = branch_name[len('refs/heads/'):] # Trim
        s = u'\u0002%s\u000F branch \u00035%s\u0003' % (ref['type'], branch_name)
        self.bot.send(s)
        logging.debug(s)

    def _process_add(self, data, ref):
        repo = data['repository']
        branch_name = ref['refId']
        if(branch_name.startswith('refs/heads/')):
            branch_name = branch_name[len('refs/heads/'):] # Trim
        s = u'New branch (%s/\u0002%s\u000F): \u0002\u00034%s\u0003\u000F' % (
            repo['project']['name'], repo['name'], branch_name, )
        self.bot.send(s)
        logging.debug(s)

def main():
    config = get_config()
    
    # Set up logging
    if(config['general']['logging']):
        logging.basicConfig(level=logging.DEBUG)
        # Set the irc client log level to DEBUG to track down connection problems.
        logging.getLogger('irc.client').setLevel(logging.WARNING)
    else:
        logging.basicConfig(level=logging.ERROR)
    
    # Set up the request handler and IRC bot
    handler = ServerHandler
    handler.bot = relay.RelayBot(config['irc client']['channel'], 
        config['irc client']['nick'], config['irc client']['server'], 
        config['irc client']['port'], password=config['irc client']['password'],
        ssl=config['irc client']['ssl'])
    handler.config = config
    # Run the IRC bot in a separate thread
    th = threading.Thread(target=handler.bot.start)
    th.daemon = True # Shut down when the main thread shuts down
    th.start()
    
    # Create and start the web server
    httpd = HTTPServer((config['web server']['addr'], config['web server']['port']), handler)
    
    print('Python stash POST server at: http://%(interface)s:%(port)s' % {
        'interface': config['web server']['addr'], 
        'port': config['web server']['port']
        })
    httpd.serve_forever()


if(__name__ == '__main__'):
    main()
